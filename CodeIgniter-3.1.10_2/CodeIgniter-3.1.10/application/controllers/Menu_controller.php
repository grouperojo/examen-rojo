<?php 
   class Menu_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  
    public function insererMenu(){
        $this->load->model('Menu');
        $plats = $this->input->post('plat'); 
        $this->Menu->insererMenu($plats);
        
        $this->load->view('accueil.php'); 
    }
    
  
   } 
?>