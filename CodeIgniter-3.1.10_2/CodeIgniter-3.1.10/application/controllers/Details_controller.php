<?php 
   class Details_controller extends CI_Controller {
	
    function __construct() { 
    parent::__construct(); 
    $this->load->helper('url'); 
    $this->load->database(); 
    } 
  
      public function index()
	{
      $this->load->model('Plat');

      $data['plats']=$this->Plat->getPlat($this->input->get('id') ); 
      $data['page']="details";
      $this->load->view('accueil.php',$data);
      
        
    }
    
  
   } 
?>