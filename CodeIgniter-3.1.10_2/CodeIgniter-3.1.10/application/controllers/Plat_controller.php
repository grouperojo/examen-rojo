<?php 
   class Plat_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 

    
      public function showPlatDuJour() { 

        $today = date("y/m/d");
        $this->load->model('Plat');
        $this->load->model('Categorie');
        $data['plats']=$this->Plat->platsDuJour($today);
        $data['categorie']=$this->Categorie->getCategorie();

        $data['page']="listePlat";
        $this->load->view('accueil.php',$data); 
         
      } 
      public function showPlatParCategorie() { 

        $today = date("y/m/d");
        $this->load->model('Plat');
        $this->load->model('Categorie');
        $categorie = $this->input->get('id');
        $data['plats']=$this->Plat->platDuJourParCategorie($categorie,$today);
        $data['categorie']=$this->Categorie->getCategorie();
        $data['page']="listePlat";
        $this->load->view('accueil.php',$data); 
      } 
      public function rechercher() { 

        $this->load->model('Plat');
        $categorie = $this->input->get('categorie');
        $data['plats']=$this->Plat->rechercher($categorie);
        $data['page']="listePlat";
        $this->load->view('accueil.php',$data); 
         
      } 
   } 
?>