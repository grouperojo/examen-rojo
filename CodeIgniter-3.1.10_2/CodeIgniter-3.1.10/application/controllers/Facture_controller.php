<?php 
   class Facture_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  
    public function payer(){
        $this->load->model('Facture');
        $idFacture = $this->input->post('idFacture'); 
        $data['factures'] = $this->Facture->getFacture($idFacture);
        $data['factures']['etat'] = 1;
        $this->Facture->payerFacture($data['factures'],$idFacture);
        
        $today = date('y/m/d');
        $this->load->model('Plat');
        $data['plats']=$this->Plat->platsDuJour($today);
        $this->load->view('accueil.php',$data); 
    }
    
  
   } 
?>