<?php 
   class Panier_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 

    
    public function index()
	{
      $this->load->model('Plat');
        $plt=$this->Plat->getPlat($this->input->get('id') ); 
        $plat=$plt[0];
        $cartitem=array(
          'id'=>$plat['idPlat'],
          'qty'=> $_POST['quantity'],
          'price'=>$plat['prix'],
          'name'=>$plat['nom'],
          'option'=>array(
              'photo'=>$plat['img']
          )
        );

        $this->cart->insert($cartitem);
        // redirect(array('Commande_controller'),'getCommand');
        $data['page']="commande";
        $today = date("y/m/d");
        $this->load->model('Plat');
        $this->load->model('Categorie');
        $data['plats']=$this->Plat->platsDuJour($today);
        $data['categorie']=$this->Categorie->getCategorie();

        $this->load->view('accueil.php',$data);
    }

    public function list(){
         $this->load->view('commande.php');
    }

   } 
?>