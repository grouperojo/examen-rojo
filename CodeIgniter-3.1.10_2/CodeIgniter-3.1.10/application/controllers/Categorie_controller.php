<?php 
   class Categorie_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  


   public function getAllCategories(){
        $this->load->model('Categorie');
        $data['categories']=$this->Categorie->getAllCategories();
        $this->load->view('accueil.php',$data); 
    }
    
   } 
?>