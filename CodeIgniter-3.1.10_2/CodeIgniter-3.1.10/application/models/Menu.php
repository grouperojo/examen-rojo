<?php 
class Menu extends CI_Model{

    private $idMenu;
    private $date;
    private $idPlat;

    /**
     * Get the value of idMenu
     */ 
    public function getIdMenu()
    {
        return $this->idMenu;
    }

    /**
     * Set the value of idMenu
     *
     * @return  self
     */ 
    public function setIdMenu($idMenu)
    {
        $this->idMenu = $idMenu;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of idPlat
     */ 
    public function getIdPlat()
    {
        return $this->idPlat;
    }

    /**
     * Set the value of idPlat
     *
     * @return  self
     */ 
    public function setIdPlat($idPlat)
    {
        $this->idPlat = $idPlat;

        return $this;
    }

    public function __construct($idMenu,$date,$idPlat){
        $this->setIdMenu($idMenu)->setDate($date)->setIdPlat($idPlat);
    }


    public function insererMenuDuJour($plat){
        $today = date("y/m/d");
        $data = array(null,$today,$plat);
        if ($this->db->insert("Commande", $data)) { 
            return true; 
         } 
    }
    public function delete($idMenu) { 
        if ($this->db->delete("stud", "roll_no = ".$roll_no)) { 
           return true; 
        } 
     } 
}
?>

