<?php 
class Personnes extends CI_Model{

    private $idPersonne;
    private $nom;
    private $mdp;
    private $profil;


    /**
     * Get the value of idpersonne
     */ 
    public function getIdPersonne()
    {
        return $this->idPersonne;
    }

    /**
     * Set the value of idpersonne
     *
     * @return  self
     */ 
    public function setIdPersonne($idpersonne)
    {
        $this->idPersonne = $idpersonne;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get the value of profil
     */ 
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set the value of profil
     *
     * @return  self
     */ 
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }
 
    // public function __construct($idpersonne,$nom,$mdp,$profil){
    //     $this->setIdPersonne($idpersonne)->setNom($nom)->setMdp($mdp)->setProfil($profil);
    // }

    public function login($username,$mdp){
        $resultat=array();
            $count=0;
            $query=$this->db->get_where('Personne',array('nom'=>$username));
            foreach($query->result() as $user){
                    $count+=1;
                    $found=false;
                    if(strcmp($user->nom,$username)==0 && strcmp($mdp,$user->mdp)==0){
                            $found=true;
                    }
                    if(!$found){
                        $resultat['errorPassword']=true;
                    }
                    else{
                        $resultat['user']=array(
                                'idPersonne'=>$user->IDPERSONNE,
                                'type'=>$user->TYPE
                        );
                    }
            }
            if($count==0){
                    $resultat['errorLogin']=true;
            }
            return $resultat;
    }
    
}
?>

