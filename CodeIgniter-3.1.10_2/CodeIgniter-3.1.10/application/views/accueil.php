<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Savory &mdash; </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by GetTemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="GetTemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="<?php echo base_url(); ?>mdb/css/fonts/family.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>mdb/css/fonts/family2.css" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>mdb/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.html">Savory <em>.</em></a></div>
                </div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
						<li class="active"><a href="<?php echo base_url(); ?>index.php/Plat_controller/showPlatDuJour">Menu</a></li>
						<li class="has-dropdown">
                            <a href="#">Categories</a>
							<ul class="dropdown">                            
                        <?php
                            foreach($categorie as $cat):
                        ?>
								<li><a href="<?php echo base_url(); ?>index.php/Plat_controller/showPlatParCategorie?id=<?php echo  $cat['nom'] ?>"><?php echo $cat['nom'] ?> </a></li>
                        <?php
                            endforeach;
                        ?>
                            </ul>
						</li>
                        <li class="btn-cta"><a href="<?php echo base_url(); ?>index.php/Panier_controller/list"><span>Voir mes commandes </span></a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/Deco_controller">Deconnexion</a></li>
                       
					</ul>	
				</div>
			</div>
			
		</div>
	</nav>
	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					

					<div class="row row-mt-15em">
						<div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">working is  <a href="http://gettemplates.co" target="_blank"> fun </a> right ? </span>
							<h1 class="cursive-font">Sell out all our menu !</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>

	
	
	<div class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 class="cursive-font primary-color">Today's Menu </h2>
					<p>To give real service you must add something which cannot be bought or measured with money, and that is sincerity and integrity.</p>
				</div>
			</div>
			<div class="row">
                <?php
                    
                    foreach($plats as $plat):
                ?>

                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <a href="images/img_1.jpg" class="fh5co-card-item image-popup">
                            <figure>
                                <div class="overlay"> <i class="ti-plus">
                                </i>
                            </div>
                                <img src="<?php echo base_url(); ?><?php echo  $plat['img']; ?>" alt="Image" class="img-responsive">
                            </figure>
                            <div class="fh5co-text">
                                <h2><?php echo $plat['nom'] ?> </h2>
                                <p><?php echo $plat['cat'] ?></p>
                                <p><span class="price cursive-font"><?php echo $plat['prix'] ?>$</span></p>
                            </div>
                        </a>
                        <div>
                            <form action="<?php echo base_url(); ?>index.php/Panier_controller?id=<?php echo $plat['idPlat'] ?>" method="post">
                                <div class="col-md-6" > <input type="text" name="quantity" class="form-control mb-4 text-right" placeholder="quantity"> </div>
                                <div class="col-md-6"> <button class="btn btn-cta my-4 btn-block text-center" type="submit">command</button> </div>
                            </form>
                            <br><br>
                        </div>  
                    </div>
                   
                <?php
                      
                        endforeach;
                ?>
			</div>
		</div>
	</div>

	<div class="gtco-cover gtco-cover-sm" style="background-image: url(images/img_bg_1.jpg)"  data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container text-center">
			<div class="display-t">
				<div class="display-tc">
					<h1>&ldquo; The best way to find yourself is to lose yourself in the service of others. &rdquo;</h1>
					<p>&mdash; Mahatma Gandhi </p>
				</div>	
			</div>
		</div>
	</div>

	

	<footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row row-pb-md">

				

				
				<div class="col-md-12 text-center">
					<div class="gtco-widget">
						<h3>Get In Touch</h3>
						<ul class="gtco-quick-contact">
							<li><a href="#"><i class="icon-phone"></i> 0346844521</a></li>
							<li><a href="#"><i class="icon-mail2"></i> ETU00752 ETU00839</a></li>
							<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>
						</ul>
					</div>
					<div class="gtco-widget">
						<h3>Get Social</h3>
						<ul class="gtco-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>

			</div>

			

		</div>
	</footer>
	<!-- </div> -->

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>mdb/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url(); ?>mdb/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>mdb/js/magnific-popup-options.js"></script>
	
	<script src="<?php echo base_url(); ?>mdb/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>mdb/js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="<?php echo base_url(); ?>mdb/js/main.js"></script>

	</body>
</html>

