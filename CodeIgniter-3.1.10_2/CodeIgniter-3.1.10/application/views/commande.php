<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Savory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by GetTemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="GetTemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/bootstrap-datetimepicker.min.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>mdb/css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.html">Savory <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
                         <li class="active"><a href="<?php echo base_url(); ?>index.php/Plat_controller/showPlatDuJour">Menu</a></li>
                         <li class="btn-cta"><a href="<?php echo base_url(); ?>index.php/Panier_controller/list"><span>Voir mes commandes </span></a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/Deco_controller">Deconnexion</a></li>
					</ul>	
				</div>
			</div>
		</div>
	</nav>
	
	<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-4 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">It's our <a href="http://gettemplates.co" target="_blank">pleasure</a> to serve</span>
							<h1 class="cursive-font">Your order</h1>	
						</div>
						<div class="col-md-8 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
							<div class="form-wrap">
								<div class="tab">
									
									<div class="tab-content">
										<div class="tab-content-inner active" data-content="signup">
											<h3 class="cursive-font"> Votre liste de commande </h3>
											<form action="#" method ="POST">
												<div>
                                                        <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">product</th>
                                                                        <th scope="col">prix</th>
                                                                        <th scope="col">quantite</th>
                                                                        <th scope="col">total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                $total=0;
                                                                        foreach($this->cart->contents() as $row):
                                                                ?>
                                                                        <tr>
                                                                                <th> <?php echo $row['name'] ?></th>
                                                                                <th> <?php echo $row['price'] ?>$</th>
                                                                                <th> <?php echo $row['qty'] ?></th>
                                                                                <th> <?php echo $row['qty']*$row['price'] ?>$</th>
                                                                        </tr>
                                                                <?php
                                                                    $total+= $row['qty']*$row['price'];
                                                                        endforeach;
                                                                ?>
                                                                        <tr>
                                                                        <th scope="col">Total</th>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"><?php echo $total?>$</th>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                </div>

												<div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" name="table" class="form-control" placeholder="La table">
                                                    </div>
													<div class="col-md-12">
                                                        <a href="#"><button class="btn btn-primary btn-block"> Facturer </button></a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary btn-block" > <a href="#" style="color:white;"> Import pdf </a></button>
													</div>
												</div>
											</form>	
										</div>

										
									</div>
								</div>
							</div>
						</div>
					</div>
							
					
				</div>
			</div>
		</div>
	</header>


	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="j<?php echo base_url(); ?>mdb/s/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>mdb/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url(); ?>mdb/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="<?php echo base_url(); ?>mdb/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>mdb/js/magnific-popup-options.js"></script>
	
	<script src="<?php echo base_url(); ?>mdb/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>mdb/js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="<?php echo base_url(); ?>mdb/js/main.js"></script>

	</body>
</html>

