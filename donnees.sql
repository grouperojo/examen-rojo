-- create database fd

create table Personne(
    idPersonne int primary key not null AUTO_INCREMENT,
    nom varchar (100),
    mdp varchar(100),
    profil varchar(50)
);

create table Categorie(
    idCategorie int primary key not null AUTO_INCREMENT ,
    nom varchar(100)
);


create table Plat(
    idPlat int primary key not null AUTO_INCREMENT ,
    idCategorie int,
    nom varchar(100),
    prix int,
    img varchar(150),
    foreign key (idCategorie) references Categorie(idCategorie)
);

create table LaTable(
    idTable int primary key not null AUTO_INCREMENT,
    numero int 
);


create table Commande(
    idCommande int primary key not null AUTO_INCREMENT,
    idPlat int,
    nombre int,
    foreign key(idPlat) references Plat(idPlat) 
);

create table Facture(
    idFacture int primary key not null AUTO_INCREMENT ,
    date Timestamp,
    somme int ,
    idTable int,
    listeCommande varchar(200),
    etat int ,
    foreign key (idTable) references LaTable(idTable)
);


create table Menu(
    idMenu int primary key not null AUTO_INCREMENT,
    date Timestamp,
    idPlat int,
    foreign key (idPlat) references Plat(idPlat)
);


create view platdujour as select plat.idPlat, categorie.nom as cat, plat.nom, plat.prix, plat.img, menu.date from plat join menu on menu.idPlat = plat.idPlat join categorie on categorie.idCategorie = plat.idCategorie;

insert into Personne values (null,'Jean','jean','serveur');
insert into Personne values (null,'Mark','mark','serveur');
insert into Personne values (null,'Peter','peter','admin');


insert into Categorie values (null,'Entree');
insert into Categorie values (null,'Resistances');
insert into Categorie values (null,'Desserts');
insert into Categorie values (null,'Boissons');

insert into LaTable values (null,1);
insert into LaTable values (null,2);
insert into LaTable values (null,3);
insert into LaTable values (null,5);


insert into Plat values (null,1,'Rouleau de printemps',12.00,'assets/img/rdp.jpeg');
insert into Plat values (null,1,'Crème de chou-fleur ',13.00,'assets/img/chou.jpg');
insert into Plat values (null,1,'Mozzarella Fritta',14.00,'assets/img/mozarella.jpg');
insert into Plat values (null,1,'Crêpe à la vietnamienne',13.00,'assets/img/crepeviet.jpg');
insert into Plat values (null,1,'Croquettes vietnamiennes',12.50,'assets/img/croquette.jpg');
insert into Plat values (null,1,'St Jacques marinés ',14.00,'assets/img/stjacques.jpg');


insert into Plat values (null,2,'Magret de canard au four ',28.00,'assets/img/canard.jpg');
insert into Plat values (null,2,'Riz aux brochettes ',25.00,'assets/img/brochette.jpg');
insert into Plat values (null,2,'Magret de canard au miel',23.50,'assets/img/magret-de-canard-au-miel-epice.jpeg');
insert into Plat values (null,2,'Saint Jacques sautees',29.00,'assets/img/34438499_p.jpg');
insert into Plat values (null,2,'Boeuf saute au basilic ',22.50,'assets/img/Boeuf-saute-au-basilic-thai-recette.jpg');


insert into Plat values (null,3,'Tarte au sucre à l érable ',6.00,'assets/img/138395-dc3c9b1.jpg');
insert into Plat values (null,3,'White chocolate cheesecake',6.00,'assets/img/e4d776ddd1b5f03e81ff7542f1ce0142.jpg');
insert into Plat values (null,3,'Gâteau chocolaté ',6.00,'assets/img/3-chocolats-sur-un-croustillant-pralinoise.jpg');
insert into Plat values (null,3,'Gateau aux Framboises',6.00,'assets/img/i82490-gateau.jpg');
insert into Plat values (null,3,'BLUE RIBBON BROWNIE',6.00,'assets/img/080f6826adc2920ec2e7c7c53ceb7ca9.jpg');
insert into Plat values (null,3,'PROFITEROLE',6.00,'assets/img/218-Profiteroles-web-horizon.jpg');


insert into Plat values (null,4,'THÉ GLACÉ ',4.00,'assets/img/fotolia_86529056_s.jpg');
insert into Plat values (null,4,'LAIT FOUETTÉ ',6.00,'assets/img/Lait-fouette-oreo_site-web.jpg');
insert into Plat values (null,4,'Mojito',9.00,'assets/img/Mojito-.jpg');
insert into Plat values (null,4,'HEINEKEN 33CL',3.00,'assets/img/heineken-33cl-jumbo-sljterij.jpg');
insert into Plat values (null,4,'DOUBLE ESPRESSO',4.00,'assets/img/Espresso-Double.jpg');

insert into Menu values (null,'2019-07-11',1);
insert into Menu values (null,'2019-07-11',3);
insert into Menu values (null,'2019-07-11',4);
insert into Menu values (null,'2019-07-11',5);
insert into Menu values (null,'2019-07-11',6);
insert into Menu values (null,'2019-07-11',10);
insert into Menu values (null,'2019-07-11',11);
insert into Menu values (null,'2019-07-11',12);
insert into Menu values (null,'2019-07-11',13);
insert into Menu values (null,'2019-07-11',14);
insert into Menu values (null,'2019-07-11',15);
insert into Menu values (null,'2019-07-11',22);